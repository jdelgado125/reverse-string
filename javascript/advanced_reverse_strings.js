// Advanced: Reverse String (Optional)
// Given the string: “Hello,World!” the reverse would be “!dlroW ,olleH”

//Part 1 - Reverse a String in JavaScript using built-in functions.
var stringToReverse = 'devslopes';
var reversed = stringToReverse.split('').reverse().join('');

console.log('Original: ', stringToReverse);
console.log('Reverse: ', reversed);

//Reverse a String in JavasScript without using built-in functions.
var stringToReverse = 'DEVSLOPES';
var reversed = '';

for (let index = stringToReverse.length - 1; index >= 0; index--) {
  reversed += stringToReverse[index];
}

console.log('Original: ', stringToReverse);
console.log('Reverse: ', reversed);